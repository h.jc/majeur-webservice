# `swaggest`, the dope framework - Technical documentation

Swaggest is a TypeScript web framework designed to make your API development faster.


For better understanding, we assume that the instanciated `swaggest` object is called `app`

## Installation

Install swaggest using the following :

```shell
npm install -g swaggest
```

## Project Initialization

Initialize your `swaggest` project using the `swaggest` command-line :
```shell
mkdir project
cd project
```

Use the `swaggest` scaffholder in interactive mode :

```
swaggest
```

or non interactive
```
swaggest init --parameter value
```

### Scaffholder parameters

| name parameter | type    | default value   |
|----------------|---------|-----------------|
| name           | string  | my-swaggest-app |
| description    | string  |                 |
| gitRepo        | srting  |                 |
| author         | string  |                 |
| needOrm        | boolean |                 |
| needLog        | boolean |                 |
| needConf       | boolean |                 |
| needApi        | boolean |                 |

## Getting started

Like `express`, `swaggest` provides a instanciator function, `swaggest()` returning an instance of Swaggest. It's a wrapper for the Swaggest class, which is a singleton. 
``` typescript
import { swaggest } from 'swaggest'

const app = swaggest()
```

## Command Line utility
### Create Class

With prompt :
```
swaggest create-class
```
Without prompt :
```
swaggest create-class --name myClass --relation myOtherClass
```


## Configuration handling
The application configuration can be loaded from three sources (in order of overriding) :
* configuration file (YAML)
* environment variables
* command line arguments

App configuration is accessible through `app.config`

Configuration file is dynamically loaded in function of en `ENV` variable. Config files are stored as YAML in the `config/` directory of a swaggest project.
First, if `ENV` variable is defined, the `<ENV>.yml` file is loaded. The `ENV` variable can be set from an environment variable or command line parameter (not yet supported).

The configuration object is simply an hashmap-like object. Every configuration element is considered as a **string**, which means that the developper must handle type conversion for configuration variables needed to be treated specifically, like for example a number. 

### Configuration file variables

All available configuration variables can be found in the `config/default.yml` file. Also, developper can define it's own config variables (which again, will always be treated as `strings`).

### Supported environment variables

Swaggest supports the fact to get configuration from environment config. Only env vars starting with `SWAGGEST_` prefix are considered. The key is lowercased, then added to the `Config` object. For example:

`SWAGGEST_PORT` will be loaded and accessible through the `config.port` variable.

### Supported command-line arguments

Not implemented yet.

## Logging

Logging is fully integrated in the swaggest framework, which provides helpers allowing users to easily declare log destinations, defaults parameters and using them in the freest possible way.

All loggers are stored in `Logger.loggers`. Every logger has a name, and is identified by this name as `Logger.loggers[name]`. Loggers are also accessible via the swaggest object as `app.loggers`.  
You can instanciate a specific logger which will register in the `Logger.loggers` store.  

Each `Logger` object has a `level` attribute, which determines from which logging level this `Logger` will process messages.
For example, a logger whose `level` is set to `INFO` will write all messages whose level is `info` or higher (`warn` and `error`). By default, the log level's default value is `info`.  
Here's a little example :
``` typescript
const app = swaggest()

new LoggerStdout('stdout', 'info') 
new LoggerFile('file', '/path/to/file.log', 'error')

app.log("it works !")
/*
  prints in console only:
    INFO : it works !
*/

app.log("that's an error !", LogLevel.ERROR)
/*
  prints in console and file :
    ERROR : that's an error !
*/

app.debug("That's debug !") 
// Won't be logged because no logger is set to "debug" level.

```

### Using loggers
You can use multiple ways to interract with your loggers :
* `app.log(message)` will log `message` to all loggers declared in you swaggest app.
* `app.log(message, 'error')` will log `message` to all loggers as an error.
* `app.log(message, 'error', 'name')` will log `message` as an error only to the logger called `name`. You can also pass a list of names if you want to log to multiple loggers
* `app.warn(message)` will log `message` to all loggers as a warning.

You can also call directly the `Logger` class, as following :
``` typescript
import { swaggest, Logger } from 'swaggest'
const app = swaggest()

Logger.loggers['stdout'].log(message, 'error')
// is equivalent to
Logger.loggers['stdout'].error(message)
// which is also equivalent to
app.log(message, 'error', 'stdout')
// which is also equivalent to
app.error(message, 'stdout')
```
This snippet will log an error message to the logger named `stdout`.

### Logger types

As said previously, every logger has a unique name which identifies it in the app instance. `swaggest` comes with two main log types, and offer you the possibility to set your own.

#### Default Logger
At app initialization, a default `LoggerStdout` is created

This basic logger is called `stdout` and comes by default when you instanciate a `swaggest` app. The log level is read from configuration as `app.config.log_level`, defined by the `log_level` config clause.

#### `LoggerStdout`
This basic logger will print messages in the program standard output.  
> **Note**: A default logger `stdout` is created at app initialization. If you can create multiple `LoggerStdout` objects, note that this will result most of the time in logging multiple time the same event in the console.

#### `LoggerFile`
The `LoggerFile` class offers features of logging messages to a file. The class constructor needs an extra parameter `filePath` which points at the file used for logging. If this file doesn't exist, it'll be created. If the parent directory of the file doesn't exist, the programm will throw a terminating error.

#### Custom Logger
Users can declare their own loggers to implement specific log mechanisms.
For example, let's imagine a `syslog` logger, looking like this :

``` typescript
import { Logger, LoggerSchema } from 'swaggest'

interface LoggerSyslogSchema extends LoggerSchema {
  ...
}

class LoggerSyslog extends Loggers implements LoggerSyslogSchema {
  ...
}
```

Note that the user will have to register himself the new logger object to the `Logger.loggers` store, and need to implement the `log()` abstract method declared in the `Logger` class.
