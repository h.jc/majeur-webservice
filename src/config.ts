const yaml = require('js-yaml');
const fs = require('fs');


export {
    Config,
    ConfigSchema
}

interface ConfigSchema {
    url: string
    port: string
    log_level: string
    env?: string
    db_host?: string
    db_port?: string
    db_user?: string
    db_passwd?: string
    db_dialect?: string
    db_database?: string
}

class Config implements ConfigSchema {
    private static instance: Config;
    public url: string = "http://localhost";
    public port: string = "8000";
    public log_level: string = 'info';
    public env?: string;
    public db_host?: string = 'localhost';
    public db_port?: string = '3306';
    public db_user?: string = 'root';
    public db_passwd?: string = 'root';
    public db_dialect?: string = 'mysql';
    public db_database?: string = 'swaggest';

    public constructor(){}

    public static getInstance(params?: ConfigSchema): Config {
        if (!this.instance) {
            this.instance = new this();
            this.instance.load(params)
        }
        return Config.instance;
    }

    protected static getEnv(): Partial<ConfigSchema> {
        let envConfig: Record<string, string> = {};
        for (let envVar in process.env) {
            if (envVar.startsWith('SWAGGEST_')) {
                let key = envVar.replace('SWAGGEST_', '').toLowerCase();
                const val = process.env[envVar];
                if (val !== undefined) {
                    envConfig[key] = val
                }
            }
        }
        return envConfig
    }

    public load(params?: ConfigSchema): Config {
        let env: string | undefined;
        if (params && params.env){
            env = params.env
        } else {
            env = process.env.SWAGGEST_ENV
        }
        if (env) {
            let configFilePath = `./config/${env}.yml`;
            let configYaml = yaml.safeLoad(fs.readFileSync(configFilePath, 'utf8'));
            Object.assign(this, configYaml)
        }
        let configEnv = Config.getEnv();
        Object.assign(this, configEnv);
        Object.assign(this, params);
        return this
    }

    public print(): void {
        console.log(JSON.stringify(Config.getInstance()))
    }
}

