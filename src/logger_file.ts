import { Logger, LogLevel, LoggerSchema } from "./logger";
const fs = require('fs')

export {
    LoggerFile,
    LoggerFileSchema
}

interface LoggerFileSchema extends LoggerSchema {
    filePath?: string
}

class LoggerFile extends Logger implements LoggerFileSchema {
    public filePath: string

    constructor(name: string, filePath: string, level?: string){
        super(name, level)
        this.filePath = filePath
        Logger.loggers[name] = this
    }

    log(message: string, level: LogLevel) {
        fs.appendFileSync(this.filePath, `${message}\n`)
    }
}
