import { Logger, LogLevel, LoggerSchema } from "./logger";

export {
    LoggerStdout,
    LoggerStdoutSchema
}

interface LoggerStdoutSchema extends LoggerSchema { }

class LoggerStdout extends Logger {

    constructor(name: string, level?: string){
        super(name, level)
        Logger.loggers[name] = this
    }

    log(message: string, level: LogLevel = this.level) {
        switch (level) {
            case LogLevel.DEBUG:    return console.debug(message)
            case LogLevel.INFO:     return console.info(message)
            case LogLevel.WARN:     return console.warn(message)
            case LogLevel.ERROR:    return console.error(message)
        }
    }
 
}