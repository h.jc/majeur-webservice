import {Config, ConfigSchema} from './config'
import {Logger, LoggerSchema, LogLevel} from './logger';
import {LoggerStdout, LoggerStdoutSchema} from './logger_stdout'
import {LoggerFile, LoggerFileSchema} from './logger_file'

export {
    app,
    Config,
    ConfigSchema,
    Logger,
    LoggerSchema,
    LogLevel,
    LoggerStdout, 
    LoggerStdoutSchema,
    LoggerFile,
    LoggerFileSchema,
    swaggest,
    Swaggest,
}


function swaggest(config?: ConfigSchema): Swaggest {
    return Swaggest.getInstance(config)
}

class Swaggest {
    private static instance: Swaggest;
    
    loggers: Record<string, Logger>;
    config: ConfigSchema;

    constructor(config?: ConfigSchema){
        this.config = Config.getInstance(config);
        this.loggers = Logger.loggers
        new LoggerStdout('stdout', this.config.log_level)
    }

    log(message: string, level?: LogLevel | string, name?: string | Array<string>) { 
        return Logger.log(message, level, name)
    }

    debug(message: string, name?: string | Array<string>): void {
        Logger.log(message, LogLevel.DEBUG, name)
    }

    info(message: string, name?: string | Array<string>): void {
        Logger.log(message, LogLevel.INFO, name)
    }
    warn(message: string, name?: string | Array<string>): void {
        Logger.log(message, LogLevel.WARN, name)
    }

    error(message: string, name?: string | Array<string>): void {
        Logger.log(message, LogLevel.ERROR, name)
    }

    public static getInstance(config?: ConfigSchema): Swaggest {
        if (!Swaggest.instance) {
            let app = new Swaggest(config);
            Swaggest.instance = app;
            return app
        }
        return Swaggest.instance
    }
}

const app = Swaggest.getInstance()
