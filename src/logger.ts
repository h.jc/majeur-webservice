export {
    Logger,
    LoggerSchema,
    LogLevel
}

enum LogLevel {
    DEBUG,
    INFO,
    WARN,
    ERROR
}

interface LoggerSchema {
    name: string
    level: LogLevel
}

abstract class Logger implements LoggerSchema {

    /**
     * List of logger associate to a name. 
     *
     * @static
     * @type {Record<string, Logger>}
     * @memberof Logger
     */
    public static loggers: Record<string, Logger> = {}

    public name: string
    public level: LogLevel = LogLevel.INFO

    constructor(name: string, level?: string) {
        if (name in Logger.loggers) {
            throw new Error(`A logger with name '${name}' is already declared`)
        }
        this.name = name
        if (level) {
            this.level = this.logLevelToEnum(level)
        }
    }

    /**
     * Function will ensure that the given level will be retrieved as an Enum LogLevel.
     * Return LogLevel.INFO if level is undefined.
     * 
     * @protected
     * @param {(string | LogLevel)} [level]
     * @returns {LogLevel} 
     * @memberof Logger
     */
    protected logLevelToEnum(level?: string | LogLevel): LogLevel {
        if (level === undefined) {
            return this.level
        }
        if (typeof level !== 'string')
            return level
        switch (level) {
            case 'debug':   return LogLevel.DEBUG
            case 'info':    return LogLevel.INFO
            case 'warn':    return LogLevel.WARN
            case 'error':   return LogLevel.ERROR
            default: 
                throw new Error(`Log level '${level}' is not supported. Allowed values are 'debug', 'info', 'warn' and 'error'`)
        }
    }

    /**
     * Function will ensure that the given level will be retrieved as a string.
     * Return 'info' if level undefined.
     *
     * @protected
     * @param {(string | LogLevel)} [level]
     * @returns {string}
     * @memberof Logger
     */
    protected logLevelToStr(level?: string | LogLevel): string {
        if (level === undefined) {
            return this.logLevelToStr(this.level)
        }
        if (typeof level === 'string')
            return level
        switch (level) {
            case LogLevel.DEBUG:    return 'debug' 
            case LogLevel.INFO:     return 'info'
            case LogLevel.WARN:     return 'warn'
            case LogLevel.ERROR:    return 'error' 
            default:
                throw new Error('Something went wrong with LogLevel enum')
        }   
    }

    /**
     * Function allows to log a message.
     * If there is no name, log in all logger.
     * Level = LogLevel.INFO by default.
     *
     * @static
     * @param {string} message
     * @param {(LogLevel | string)} [level=LogLevel.INFO]
     * @param {(string | Array<string>)} [name]
     * @memberof Logger
     */
    static log(message: string, level: LogLevel | string = LogLevel.INFO, name?: string | Array<string>): void {
        let logTo = this.get_loggers(name)
        logTo.forEach(logger => {
            if (typeof level === 'string'){
                level = logger.logLevelToEnum(level)
            }  
            // allow to display the message if level is more or as much importante than the level of the logger
            if (level >= logger.level){
                logger.log(logger.format(message, level), level)
            }
        })
    }
    
    /**
     * Function return a list of logger corresponding to the list of name.
     * Return list of all logger if there is no name.
     *
     * @static
     * @param {(Array<string> | string)} [name]
     * @returns {Array<Logger>}
     * @memberof Logger
     */
    static get_loggers(name?: Array<string> | string): Array<Logger> {
        let loggers: Array<Logger> = []
        if (name === undefined) {
            Object.entries(Logger.loggers).forEach(([name, logger]) => {
                loggers.push(Logger.loggers[name])
            })
        }
        if (typeof name === 'string') {
            if (name in Logger.loggers){
                loggers.push(Logger.loggers[name])
            } else {
                throw new Error(`Logger '${name}' doesn't exist`)
            }
        }
        if (name instanceof Array) {
            name.forEach((name) => {
                if (name in Logger.loggers) {
                    loggers.push(Logger.loggers[name])
                } else {
                    throw new Error(`Logger '${name}' doesn't exist`)
                }
            })
        }
        return loggers
    }
    
    debug(message: string): void {
        this.log(message, LogLevel.DEBUG)
    }

    info(message: string): void {
        this.log(message, LogLevel.INFO)
    }
    warn(message: string): void {
        this.log(message, LogLevel.WARN)
    }

    error(message: string): void {
        this.log(message, LogLevel.ERROR)
    }
    
    abstract log(message: string, level: LogLevel): void

    /**
     * Function allows to display a message with a certain structure
     *
     * @param {string} message
     * @param {LogLevel} [level]
     * @returns {string}
     * @memberof Logger
     */
    format(message: string, level: LogLevel): string {
        let date = Date.now().toString()
        let logLevel = this.logLevelToStr(level).toUpperCase()
        return `${date} - ${logLevel}: ${message}`
    }
}
