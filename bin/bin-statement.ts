/**
 * return Sorts array if statement match case for us generating app
 * @param argument
 * @return string[] retourne un array contenant les arguments pour hygen
 */
export function statement(argument: string[]): string[] {
    // On vérifie si l'utilisateur à tapé des arguments
    if (argument.length === 0) {
      argument.push('init', 'with-prompt');
      return argument;
    } else {
      switch (argument[0]) {
        case 'init':
          // Si il à tapé des arguments on ne les utilisent pour la génération des fichiers
            argument.splice(1, 0, 'new');
            return argument;
        case 'create-class':
          if (argument.length === 1) {
            argument.splice(1, 0, 'with-prompt');
          } else {
            argument.splice(1, 0, 'new');
          }
          return argument;
        default:
            return [];
          console.log('Désolé, l\'argument : <' + argument + '> n\'existe pas, veuillez consulter --help');
          return [];
      }
    }
  }