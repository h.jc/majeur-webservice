#!/usr/bin/env node
 
const program = require('commander');
 
program
  .version('0.1.0')
  .option('swaggest', 'Initialisation project')
  .option('--name [name]', 'Name of the project')
  .option('--description [description]', 'Description of the project ')
  .option('--gitRepo [gitRepo]', 'gitRepo of the project ')
  .option('--author [author]', 'author of the project ')
  .option('--needORM', 'Add ORM in project ')
  .option('--needLog ', 'Add Log in project ')
  .option('--neesConf', 'Add Conf in  project ')
  .option('--needApi ', ' Add API in project ')
 
// must be before .parse() since
// node's emit() is immediate
 
program.on('--help', function(){
  console.log('')
  console.log('Examples:');
  console.log('  $ swaggest --help');
  console.log('  $ swaggest -h');
});
 
program.parse(process.argv);
 
console.log('stuff');