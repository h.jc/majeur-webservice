---
inject: true
to: ./src/models/<%= relation %>.ts
after: "relation: {"
skip_if: <% relation %>
---
            <%= name %> :{
                type: RelationType.BelongsTo,
                model: <%= h.capitalize(name) %>,
                foreignKey: '',
            },