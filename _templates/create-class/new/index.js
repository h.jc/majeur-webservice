module.exports = {
  params: ({ args }) => {
    return { 
      name: args.name,
      relation: args.relation
    }
  }
}