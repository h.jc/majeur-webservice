---
inject: true
to: ./src/models/<%= relation %>.ts
skip_if: <% relation %>
---
import { <%= h.capitalize(name) %> } from './<%= name %>'