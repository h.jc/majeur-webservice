---
to: ./src/models/<%= name %>.ts
---
import { Model, ModelConfig, RelationType, Relation } from 'ORM/src/models/model';
<% if(relation){  -%> import { <%= h.capitalize(relation) %> } from './<%= relation %>'; <% } %>


export interface <%= h.capitalize(name) %>Schema {
    id?: string | number;
}

export class <%= h.capitalize(name) %> extends Model implements <%= h.capitalize(name) %>Schema {

    static config: ModelConfig = {
        endpoint: '/<%= name %>/', 
        relation: { <% if(relation){ %>
            <%= relation %>: {
                type: RelationType.BelongsTo,
                model: <%= h.capitalize(relation) %>,
            }
            <% } %>
            <% if(relation == name){ %>
                <%= name %>: {
                    type: RelationType.HasMany,
                    model: <%= h.capitalize(name) %>
                }
            <% } %>
        } as any, 
        // ## } as Record<string, Relation>, 
    }
    static id?: string | number;

}
