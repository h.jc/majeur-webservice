---
to: ./package.json
---
{
  "name": "<%= name || 'my-swaggest-app' %>",
  "version": "1.0.0",
  "description": "<%= description %>",
  "main": "index.js",
  "scripts": {
    "build": "npm explore ORM -- tsc ; tsc ",
    "postinstall": "npm run build"
  },
<% if(gitRepo){ -%>
  "repository": {
    "type": "git",
    "url": "<%= gitRepo %>"
  },
<% } -%>
  "author": "<%= author %>",
  "license": "ISC",
  "dependencies": { <%
    if(locals.needOrm){ %>
    "ORM": "git+https://ORM_ACCESS:DayeCisozX8gC3Gbx4JC@gitlab.com/swaggest1/orm#master",<%
    } -%>
    "swaggest": "git+https://ORM_ACCESS:DayeCisozX8gC3Gbx4JC@gitlab.com/swaggest1/swaggest",
    "ts-node": "^8.6.2",
    "tslib": "^1.10.0",
    "typescript": "^3.7.5"
  }
}