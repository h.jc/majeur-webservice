
// see types of prompts:
// https://github.com/enquirer/enquirer/tree/master/examples
//
module.exports = [
  {
    type: 'input',
    name: 'name',
    message: "name: "
  },
  {
    type: 'input',
    name: 'description',
    message: "description: "
  },
  {
    type: 'input',
    name: 'gitRepo',
    message: "git repository: "
  },
  {
    type: 'input',
    name: 'author',
    message: "author: "
  },
  {
    type: 'confirm',
    name: 'needOrm',
    message: "Do you need an ORM ?"
  },
];
