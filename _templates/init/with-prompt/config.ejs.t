---
to: ./config/default.yml
---
url: localhost
port: 8000
db_host: localhost
db_port: 3306
db_dialect: mysql
db_user: swaggest
db_passwd: secret
db_database: test
