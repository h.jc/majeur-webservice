---
to: ./demo.ts
---
import { app, Swaggest, LoggerFile, LogLevel } from "swaggest/dist/src/swaggest";
import {User} from './example';

function logDemo(application: Swaggest): void {

    application.log("Hello, I'm info by default")
    application.error("I'm an error")

    new LoggerFile('file', '/tmp/swaggest.log', 'warn')

    application.info("I'm in console only, because file is on 'warn' level")
    application.warn("I'm in file only, because I'm limited to logger named 'file'", 'file')
    application.debug("I'm nowhere, because no logger is set to 'debug' level")

    application.loggers['stdout'].level = LogLevel.DEBUG
    application.debug("I'm in console now !")
}

function configDemo(app: Swaggest): void {
    console.log(app.config)
}

async function ORMDemo(): Promise<void>{
    const user = await User.findById(1);
    console.log(user);
}

const application: Swaggest = app;

configDemo(application);

logDemo(application);

ORMDemo();