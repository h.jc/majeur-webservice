---
to: ./example.ts
---
import {
  Model, ModelConfig,
} from 'ORM/src/models/model';

export interface UserSchema {
  id: string | number;
  name: string;
  username: string;
}

export class User extends Model implements UserSchema {
  id!: string | number;

  static config: ModelConfig = {
    endpoint: 'user',
  };

  name!: string;

  username!: string;
}
