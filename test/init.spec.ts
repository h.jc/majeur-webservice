import 'mocha';
import {promises as fs} from 'fs';
import expect from 'expect';
import {createClass} from './utils/create-class';
import {deleteClass} from './utils/delete-class';
import {statement} from '../bin/bin-statement';


describe('Generating good arguments', function() {
  describe('bin.ts', function() {

    it('should generate app with-prompt', function() {
      const argument = statement([]);
      expect(argument).toStrictEqual(['init', 'with-prompt']);
    });

    it('should generate app without prompt', function() {
      const argument = statement(['init']);
      expect(argument).toStrictEqual(['init', 'new']);
    });

    it('should generate app without prompt with params', function() {
      const argument = statement(['init', '--name', 'my swaggest app', 'author', 'my swaggest team']);
      expect(argument).toStrictEqual(['init', 'new', '--name', 'my swaggest app', 'author', 'my swaggest team']);
    });

    it('should generate class with prompt', function() {
      const argument = statement(['create-class']);
      expect(argument).toStrictEqual(['create-class', 'with-prompt']);
    });

    it('should generate class named "myClass" without prompt', function() {
      const argument = statement(['create-class', '--name', 'myClass']);
      expect(argument).toStrictEqual(['create-class', 'new', '--name', 'myClass']);
    });
  });
});

describe('Generating good files', function() {
  describe('bin.ts', function() {
    before('create class', async function() {
       await createClass('mySwaggestClass');
    });

    it('file contain my name class', async function() {
      const pathRoot = "src/models/mySwaggestClass.ts";
      await fs.readFile(pathRoot, "utf-8").then( data => {
        expect(data).toContain('Myswaggestclass');
      }).catch(err => {
        console.log(err);
        expect(err).toBeFalsy();
      });
    });

    after('delete class', async function() {
       await deleteClass('mySwaggestClass');
    });

  });
});