import {statement} from '../../bin/bin-statement';
import {run} from '../../bin/bin';

export async function createClass(name: string, relation?:string){

    let args: string[] = ['create-class', '--name', name];
    if(relation){
        args.push('--relation', relation)
    }
    await run(statement(args));
    console.log('createClass')
}